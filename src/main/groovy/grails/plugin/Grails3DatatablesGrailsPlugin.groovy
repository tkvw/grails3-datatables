package grails3.datatables

import grails.plugins.*

class Grails3DatatablesGrailsPlugin extends Plugin {

    // the version or versions of Grails the plugin is designed for
    def grailsVersion = "3.1.8 > *"
    // resources that are excluded from plugin packaging
    def pluginExcludes = [
        "grails-app/views/error.gsp"
    ]

    // TODO Fill in these fields
    def title = "Grails Datatables" // Headline display name of the plugin
    def author = "Ben Wilson"
    def authorEmail = "ben.wilson@icsynergy.com"
    def description = '''\
This plugin allows you to quickly add feature-rich tables to your Grails application. It uses the excellent DataTables plugin for jQuery created by SpryMedia Ltd.

This plugin provides the following components to your application:
- A taglib that you use to define the table in your GSP. The taglib generates all the HTML and JavaScript necessary to create the table.
- A controller and services that provide data for the table.
- An extensible reporting system.
'''
    def profiles = ['web']

    // URL to the plugin's documentation
    def documentation = "http://grails.org/plugin/grails3-datatables"

    // Extra (optional) plugin metadata

    // License: one of 'APACHE', 'GPL2', 'GPL3'
    def license = "APACHE"

    // Location of the plugin's issue tracker.
    def issueManagement = [ system: "BITBUCKET", url: "https://bitbucket.org/ben-wilson/grails3-datatables/issues" ]

    // Online location of the plugin's browseable source code.
    def scm = [ url: "https://bitbucket.org/ben-wilson/grails3-datatables/src" ]

    Closure doWithSpring() { {->
            // TODO Implement runtime spring config (optional)
        }
    }

    void doWithDynamicMethods() {
        // TODO Implement registering dynamic methods to classes (optional)
    }

    void doWithApplicationContext() {
        // TODO Implement post initialization spring config (optional)
    }

    void onChange(Map<String, Object> event) {
        // TODO Implement code that is executed when any artefact that this plugin is
        // watching is modified and reloaded. The event contains: event.source,
        // event.application, event.manager, event.ctx, and event.plugin.
    }

    void onConfigChange(Map<String, Object> event) {
        // TODO Implement code that is executed when the project configuration changes.
        // The event is the same as for 'onChange'.
    }

    void onShutdown(Map<String, Object> event) {
        // TODO Implement code that is executed when the application shuts down (optional)
    }
}
